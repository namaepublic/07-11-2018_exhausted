﻿using UnityEngine;

public class PatternCreator : MonoBehaviour
{
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private BoxCollider2D _collider;
    [SerializeField] private GameObject _groundPrefab;
    [SerializeField] private GameObject _spikesPrefab;
    [SerializeField] private GameObject _cloudPrefab;
    [SerializeField] private float _groundNbr;
    [SerializeField] private float _spikesNbr;
    [SerializeField] private float _cloudNumber;
    [SerializeField] private Vector2 _minBoundaries, _maxBoundaries;
    [SerializeField] private Vector2 _minSize, _maxSize;

    private void OnValidate()
    {
        _playerController = FindObjectOfType<PlayerController>();
        _collider = GetComponent<BoxCollider2D>();
    }

    private void Start()
    {
        CreatePattern();
    }

    public void CreatePattern()
    {
        _collider.offset = _playerController.transform.position + Vector3.right * 200;
        CreateGrounds();
        CreateSpikes();
        CreateClouds();
    }

    private void CreateGrounds()
    {
        for (var i = 0; i < _groundNbr; i++)
        {
            var go = Instantiate(_groundPrefab, _playerController.transform.position + CreateRandomPosition(), Quaternion.identity,
                transform);
            go.transform.localScale = CreateRandomScale();
        }
    }

    private void CreateSpikes()
    {
        for (var i = 0; i < _spikesNbr; i++)
            Instantiate(_spikesPrefab, _playerController.transform.position + CreateRandomPosition(), Quaternion.identity, transform);
    }

    private void CreateClouds()
    {
        for (var i = 0; i < _cloudNumber; i++)
        {
            var go = Instantiate(_cloudPrefab, _playerController.transform.position + CreateRandomPosition(), Quaternion.identity,
                transform);
            go.transform.localScale = CreateRandomScale();
        }
    }

    private Vector3 CreateRandomPosition()
    {
        return new Vector2(Random.Range(_minBoundaries.x, _maxBoundaries.x), Random.Range(_minBoundaries.y, _maxBoundaries.y));
    }

    private Vector2 CreateRandomScale()
    {
        return new Vector2(Random.Range(_minSize.x, _maxSize.x), Random.Range(_minSize.y, _maxSize.y));
    }
}