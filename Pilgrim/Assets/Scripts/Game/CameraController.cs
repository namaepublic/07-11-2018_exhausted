﻿using System;
using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class CameraController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private PlayerController _playerController;
    [SerializeField] private Transform _background;
    [SerializeField] private Vector3 _offset;
    [SerializeField] [Range(0, 1)] private float _smoothXTime;
    [SerializeField] [Range(0, 1)] private float _smoothYTime;
    [SerializeField] private float _zoomCoef = 0.1f;
    [SerializeField] private float _zoomMin, _zoomMax;
    [SerializeField] [Range(0, 1)] private float _zoomSmoothTime;

    [SerializeField] [HideInInspector] private float _scaleRatio;

#if UNITY_EDITOR
    private void OnValidate()
    {
        _camera = GetComponent<Camera>();
        _playerController = FindObjectOfType<PlayerController>();
        if (_background != null)
            _scaleRatio = _background.localScale.x / _camera.orthographicSize;
    }

    private void Update()
    {
        if (!EditorApplication.isPlaying)
            FixedUpdate();
    }
#endif

    private void FixedUpdate()
    {
        CameraMove();
        CameraZoom();
    }

    private void CameraMove()
    {
        var velocity = 0f;
        var position = transform.position;
        position.x = Mathf.SmoothDamp(position.x, (_playerController.transform.position + _offset).x, ref velocity, _smoothXTime);
        velocity = 0f;
        position.y = Mathf.SmoothDamp(position.y, (_playerController.transform.position + _offset).y, ref velocity, _smoothYTime);
        transform.position = position;
    }

    private void CameraZoom()
    {
        var velocity = 0f;
        switch (_playerController.Behaviour)
        {
            case Behaviour.GLIDE:
                _camera.orthographicSize = Mathf.SmoothDamp(_camera.orthographicSize, _zoomMax, ref velocity, _zoomSmoothTime);
                break;
            case Behaviour.SOAR:
            case Behaviour.DIVE:
                _camera.orthographicSize = Mathf.SmoothDamp(_camera.orthographicSize,
                    Mathf.Clamp(_playerController.Velocity * _zoomCoef, _zoomMax, _zoomMin), ref velocity, _zoomSmoothTime);
                break;
        }

        if (_background != null)
            _background.localScale = Vector3.one * _camera.orthographicSize * _scaleRatio;
    }
}