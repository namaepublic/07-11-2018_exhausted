﻿using System.Globalization;
using TMPro;
using UnityEngine;

public class ScoreController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private float _scoreFactor = 1;
    [SerializeField] private int _numberOfDecimals = 2;

    private float _score;

    public float Score
    {
        get { return _score; }
        set
        {
            var factor = Mathf.Pow(10, _numberOfDecimals);
            _score = Mathf.Round(value * _scoreFactor * factor) / factor;
            _scoreText.text = _score.ToString(CultureInfo.InvariantCulture);
        }
    }
}