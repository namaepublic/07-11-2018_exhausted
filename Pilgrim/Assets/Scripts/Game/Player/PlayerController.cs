﻿using System;
using UnityEngine;
using UnityEngine.UI;

public enum Behaviour
{
    GLIDE,
    SOAR,
    DIVE
}

public class PlayerController : MonoBehaviour
{
    [SerializeField] private GameController _gameController;
    [SerializeField] private ScoreController _scoreController;
    [SerializeField] private PatternCreator _patternCreator;
    [SerializeField] private Rigidbody2D _rigidbody2D;

    [Header("Movements")] [SerializeField] private Vector3 _startPosition;
    [SerializeField] private Vector2 _soarDirection, _diveDirection;
    [SerializeField] private float _soarSpeed, _diveSpeed;
    [SerializeField] private Vector2 _maxGlideVelocity, _maxSoarVelocity, _maxDiveVelocity;
    [SerializeField] private float _minVelocityToDive;
    [SerializeField] [Range(0, 1)] private float _glideVelocitySmoothTime;

    [Header("Rotations")] [SerializeField] [Range(0, 1)]
    private float _rotationSmoothTime;

    [SerializeField] private float _soarAngle, _diveAngle, _glideAngle;

    [Header("Energy")] [SerializeField] private int _maxEnergy;
    [SerializeField] [Range(0, 100)] private int _minPercentEnergyToSoar;
    [SerializeField] private int _glideRestoreValue;
    [SerializeField] private float _glideRestoreCooldown;
    [SerializeField] private int _diveRestoreValue;
    [SerializeField] private float _diveRestoreCooldown;
    [SerializeField] private int _consumeValue;
    [SerializeField] private float _consumeCooldown;
    [SerializeField] private Image _energyBarFill, _minEnergyFill;

    [SerializeField] [HideInInspector] private int _spikesLayer, _patternLayer;

    private int _energy;
    private bool _noEnergyReached;
    private float _currentTimer;

    public Behaviour Behaviour { get; private set; }

    public float Velocity
    {
        get { return _rigidbody2D.velocity.magnitude; }
    }

    private int Energy
    {
        get { return _energy; }
        set
        {
            _energy = Mathf.Clamp(value, 0, _maxEnergy);
            if (_energy == 0)
                _noEnergyReached = true;
            _energyBarFill.fillAmount = (float) _energy / _maxEnergy;
        }
    }

    private void OnValidate()
    {
        _gameController = FindObjectOfType<GameController>();
        _scoreController = FindObjectOfType<ScoreController>();
        _patternCreator = FindObjectOfType<PatternCreator>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _spikesLayer = LayerMask.NameToLayer("Spikes");
        _patternLayer = LayerMask.NameToLayer("Pattern");
        transform.position = _startPosition;
    }

    private void Start()
    {
        Init();
    }

    private void Update()
    {
        _currentTimer += Time.deltaTime;
        switch (Behaviour)
        {
            case Behaviour.GLIDE:
                if (_currentTimer >= _glideRestoreCooldown)
                    RestoreEnergy(_glideRestoreValue);
                break;
            case Behaviour.SOAR:
                if (_currentTimer >= _consumeCooldown)
                    ConsumeEnergy();
                break;
            case Behaviour.DIVE:
                if (_currentTimer >= _diveRestoreCooldown)
                    RestoreEnergy(_diveRestoreValue);
                break;
        }

        _scoreController.Score = transform.position.x - _startPosition.x;
    }

    private void FixedUpdate()
    {
        switch (Behaviour)
        {
            case Behaviour.GLIDE:
                Glide();
                break;
            case Behaviour.SOAR:
                Soar();
                break;
            case Behaviour.DIVE:
                Dive();
                break;
        }
    }

    public void Init()
    {
        _currentTimer = 0;
        Energy = _maxEnergy;
        Behaviour = Behaviour.GLIDE;
        _minEnergyFill.fillAmount = (float) _minPercentEnergyToSoar / 100;
    }

    public void StartBehaviour(ClickZone clickZone)
    {
        switch (clickZone)
        {
            case ClickZone.LEFT:
                if (_noEnergyReached && _energy >= _maxEnergy * (float) _minPercentEnergyToSoar / 100 ||
                    !_noEnergyReached && _energy >= 0)
                {
                    _noEnergyReached = false;
                    Behaviour = Behaviour.SOAR;
                }
                else
                    StopBehaviour();

                break;
            case ClickZone.RIGHT:
                if (Math.Abs(_rigidbody2D.velocity.magnitude) > _minVelocityToDive)
                    Behaviour = Behaviour.DIVE;
                else
                    StopBehaviour();
                break;
        }
    }

    public void StopBehaviour()
    {
        Behaviour = Behaviour.GLIDE;
    }

    private Vector2 ClampVelocity(Vector2 maxVelocity)
    {
        if (_rigidbody2D.velocity.x > maxVelocity.x)
            return new Vector2(maxVelocity.x, _rigidbody2D.velocity.y);

        if (Mathf.Abs(_rigidbody2D.velocity.y) > Mathf.Abs(maxVelocity.y))
            return new Vector2(_rigidbody2D.velocity.x, maxVelocity.y);

        return _rigidbody2D.velocity;
    }

    private void Glide()
    {
        var velocity = 0f;
        var velocity2 = Vector2.zero;

        _rigidbody2D.velocity = Vector2.SmoothDamp(_rigidbody2D.velocity,
            ClampVelocity(new Vector2(_rigidbody2D.velocity.x, -_maxGlideVelocity.y)), ref velocity2, _glideVelocitySmoothTime);
        velocity2 = Vector2.zero;

        _rigidbody2D.velocity = Vector2.SmoothDamp(_rigidbody2D.velocity,
            ClampVelocity(new Vector2(_rigidbody2D.velocity.x, _maxGlideVelocity.y)), ref velocity2, _glideVelocitySmoothTime);
        velocity2 = Vector2.zero;

        _rigidbody2D.velocity = Vector2.SmoothDamp(_rigidbody2D.velocity,
            ClampVelocity(new Vector2(_maxGlideVelocity.x, _rigidbody2D.velocity.y)), ref velocity2, _glideVelocitySmoothTime);

        _rigidbody2D.rotation = Mathf.SmoothDamp(_rigidbody2D.rotation, _glideAngle, ref velocity, _rotationSmoothTime);
    }

    private void Soar()
    {
        var velocity = 0f;

        if (_rigidbody2D.velocity.y < _maxSoarVelocity.y)
            _rigidbody2D.AddForce(_soarDirection * _soarSpeed * Time.deltaTime, ForceMode2D.Force);

        _rigidbody2D.velocity = ClampVelocity(_maxSoarVelocity);

        _rigidbody2D.rotation = Mathf.SmoothDamp(_rigidbody2D.rotation, _soarAngle, ref velocity, _rotationSmoothTime);
    }

    private void Dive()
    {
        var velocity = 0f;

        if (_rigidbody2D.velocity.y > _maxDiveVelocity.y)
            _rigidbody2D.AddForce(_diveDirection * _diveSpeed * Time.deltaTime, ForceMode2D.Force);

        _rigidbody2D.velocity = ClampVelocity(_maxDiveVelocity);

        _rigidbody2D.rotation = Mathf.SmoothDamp(_rigidbody2D.rotation, _diveAngle, ref velocity, _rotationSmoothTime);
    }

    private void RestoreEnergy(int value)
    {
        _currentTimer = 0;
        Energy += value;
    }

    private void ConsumeEnergy()
    {
        _currentTimer = 0;
        Energy -= _consumeValue;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == _spikesLayer) _gameController.GameOver();
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == _patternLayer) _patternCreator.CreatePattern();
    }
}