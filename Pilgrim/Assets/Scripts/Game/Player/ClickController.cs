﻿using UnityEngine;

public enum ClickZone
{
	LEFT,
	RIGHT
}

public class ClickController : MonoBehaviour
{
	[SerializeField] private ClickZone _clickZone;
	[SerializeField] private PlayerController _playerController;

	private void OnValidate()
	{
		_playerController = FindObjectOfType<PlayerController>();
	}

	private void OnMouseDrag()
	{
		_playerController.StartBehaviour(_clickZone);
	}

	private void OnMouseUp()
	{
		_playerController.StopBehaviour();
	}
}
